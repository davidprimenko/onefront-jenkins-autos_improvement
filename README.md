# Onefront Jenkins Autos

This repository contains scripts responsible to:
* Create/Update Jenkins Volumes
* Get/Update Jenkins Docker Image
* Set settings to a Jenkins Volume
* Create the docker-compose.yml file

## Installing requirements
Python 3.x with pip3 is needed. 

```$ pip3 install -r requirements.txt```

## Get/Update Jenkins Image

```$ python3 init_jenkins.py --updateDockerImage=true```

## Create/Update Jenkins Volumes (Needs Admin Privileges)
Note: The update use as default the jenkins_home

```$ sudo python3 init_jenkins.py --createUpdateVolumes=true```

### Create docker-compose config file 
You can use the --createDockerComposeWithAuthServer to create it with the Authentication web-service

```
$ python3 init_jenkins.py --createDockerCompose=true
$ python3 init_jenkins.py --createDockerComposeWithAuthServer=true
```

### Update only Xml configs
If you change anything from default_config.json, just execute:

```$ python3 init_jenkins.py --updateXmlCfgs=true```


## Add user to docker group (use docker-compose without SUDO)
```$ usermod -a -G docker $USER```

## Start Jenkins
Note: You might need sudo. In the folder that contains the docker-compose.yml file:

```$ docker-compose up ```

## Jobs scripts
Inside jobs_scripts, there are scripts that are executed by Jenkins jobs.

## Playground Instance
There is a instance on http://mad-onefront-02.odigeo.org:48005/ which is available for testing new jobs/settings. It only use only up to 1 Amazon Instance. 
It's called playground. Have fun.

## TODO

* Redirect link: http://mad-onefront-02.odigeo.org/playground to http://mad-onefront-02.odigeo.org:48005/
* Improve Onefront-Autos-Main results - Is quite messy how the results are presented
* Users - Use onefront login system
