#!/usr/bin/env python3

from argparse import ArgumentParser
import urllib.request
import tarfile
import yaml
import json
import docker
import sys
import os
import configparser
import update_volumes as updateVolumes
OKGREEN = '\033[92m'
WARNING = '\033[93m'
FAIL = '\033[91m'
ENDC = '\033[0m'
CONFIG_FILE='config.ini'

if not os.path.exists(CONFIG_FILE):
    print(FAIL + ">> Missing the config.ini file" + ENDC)
    sys.exit(0)
config = configparser.ConfigParser()
config.read(CONFIG_FILE)

BASE_PATH_VOLUMES = config['DEFAULT']['BASE_PATH_VOLUMES']
PATH_VOLUMES = BASE_PATH_VOLUMES + '/Jenkins_Volumes'
BASE_VOLUME = BASE_PATH_VOLUMES + '/onefront-jenkins-autos'
DEFAULT_JENKINS_HOME='/default_jenkins_home'
DOCKER_CFG_PATH = os.path.expanduser('~')
DOCKER_LOGIN_AUTH = config['DEFAULT']['DOCKER_LOGIN_AUTH']
DOCKER_COMPOSE_FILENAME = 'docker-compose.yml'
AUTHENTICATION_SERVER_TAG = 'AUTH_server_instance'
AUTHENTICATION_SERVER_PORT = config['DEFAULT']['AUTHENTICATION_SERVER_PORT']
AUTHENTICATION_SERVER_IMAGE = config['DEFAULT']['AUTHENTICATION_SERVER_IMAGE']
PORTAINER_SERVICE_TAG = 'portainer'

VOLUMES = [
    'CONVERT_instance',
    'ENGAGE_instance',
    'ENHANCE_instance',
    'RM_instance',
    'PLAYGROUND_instance'
]

JENKINS_COMMON_VOLUMES = [
    '/etc/timezone:/etc/timezone',
    '/etc/localtime:/etc/localtime'
]

JENKINS_EXPOSED_PORTS = {
    "port": 8080,
    "port_2": 50000
}

JENKINS_IMAGE = '781302463931.dkr.ecr.eu-west-1.amazonaws.com/jenkins-madrid:latest'
JENKINS_HOME = '/var/jenkins_home'

CAP_ADD = [
    'SYS_TIME'
]

DOCKER_COMPOSE_VERSION_TAG = 'version'
DOCKER_COMPOSE_VERSION = '3'
DOCKER_COMPOSE_SERVICES_TAG = 'services'
DOCKER_COMPOSE_PORTS_TAG = 'ports'
DOCKER_COMPOSE_IMAGE_TAG = 'image'
DOCKER_COMPOSE_VOLUMES_TAG = 'volumes'
DOCKER_COMPOSE_COMMAND_TAG = 'command'
DOCKER_COMPOSE_CAP_ADD_TAG = 'cap_add'

PORTAINER_CONFIG = {
    DOCKER_COMPOSE_IMAGE_TAG  : 'portainer/portainer',
    DOCKER_COMPOSE_COMMAND_TAG : '-H unix:///var/run/docker.sock',
    DOCKER_COMPOSE_PORTS_TAG: ['9000:9000'],
    DOCKER_COMPOSE_VOLUMES_TAG : ['/var/run/docker.sock:/var/run/docker.sock', '{}:{}'.format(BASE_PATH_VOLUMES + '/portainer', '/data')]
}

JSON_VOLUMES_CFG_FILE = updateVolumes.JSON_VOLUMES_CFG_FILE


def parseArgs():
    parser = ArgumentParser()
    parser.add_argument('--createDockerCompose', dest='dockerCompose', type=bool, 
                        help='Flag to create docker-compose.yml')
    parser.add_argument('--createDockerComposeWithAuthServer', dest='dockerComposeAuth', type=bool,
                        help='Flag to create docker-compose.yml with Authentication Server')
    parser.add_argument('--createUpdateVolumes', dest='updateVolumes', type=bool,
                        help='Flag to create or update docker volumes')
    parser.add_argument('--updateDockerImage', dest='updateDockerImage', type=bool, 
                        help='Update docker Jenkins image')
    parser.add_argument('--extractJenkinsHome', dest='extractJenkinsHome', type=bool,
                        help='Extract Jenkins home')
    parser.add_argument('--updateXmlCfgs', dest='updateXmlCfgs', type=bool, 
                        help='Update only volumes cfgs')

    return parser.parse_args()


def extractJenkinsHome():
    tar = tarfile.open('jenkins_home.tar.gz')
    tar.extractall(path=BASE_VOLUME)
    tar.close()

def updateDockerImage():
    urllib.request.urlretrieve(DOCKER_LOGIN_AUTH, 'docker_login.tar.gz')
    tar = tarfile.open('docker_login.tar.gz', 'r:gz')
    tar.extractall(DOCKER_CFG_PATH)
    tar.close()
    client = docker.from_env()
    client.images.pull(JENKINS_IMAGE) 

def getCapAdd():
    capAdd = []
    for cap in CAP_ADD:
        capAdd.append(cap)
    return capAdd

def getPorts(volumeName):
    portsMap = []
    jsonData = json.load(open(JSON_VOLUMES_CFG_FILE, 'r+'))
    for vol in jsonData['Volumes']:
        if vol['name'] == volumeName:
            portsMap.append('{}:{}'.format(vol['port'],JENKINS_EXPOSED_PORTS['port']))
            portsMap.append('{}:{}'.format(vol['port_2'],JENKINS_EXPOSED_PORTS['port_2']))
    return portsMap 

def writeYMLFile(filename, data):
    with open(filename, 'w') as out:
        yaml.dump(data, out, default_flow_style=False)

def generateDockerComposeFile(filename, withAuth, portainer=True):
    jsonVolumes = {}
    for vol in VOLUMES: 
        jsonVolumes[vol] = {
            DOCKER_COMPOSE_IMAGE_TAG: JENKINS_IMAGE,
            DOCKER_COMPOSE_VOLUMES_TAG: JENKINS_COMMON_VOLUMES + ['{}/{}:{}'.format(PATH_VOLUMES, vol, JENKINS_HOME)],
            DOCKER_COMPOSE_CAP_ADD_TAG: getCapAdd(),
            DOCKER_COMPOSE_PORTS_TAG: getPorts(vol)
        }

    if (withAuth):
        jsonVolumes[AUTHENTICATION_SERVER_TAG] = {
            DOCKER_COMPOSE_PORTS_TAG: ['{}:{}'.format(AUTHENTICATION_SERVER_PORT, AUTHENTICATION_SERVER_PORT)],
            DOCKER_COMPOSE_IMAGE_TAG: AUTHENTICATION_SERVER_IMAGE
        }

    if(portainer):
        jsonVolumes[PORTAINER_SERVICE_TAG] = PORTAINER_CONFIG

    jsonDockerCompose = {
        DOCKER_COMPOSE_VERSION_TAG: DOCKER_COMPOSE_VERSION,
        DOCKER_COMPOSE_SERVICES_TAG: jsonVolumes
    }


    writeYMLFile(filename, jsonDockerCompose)


if __name__ == '__main__':
    ARGS = parseArgs()
    if ARGS.updateDockerImage:
        print('>> Updating Docker Image {}'.format(JENKINS_IMAGE))
        updateDockerImage()
    if ARGS.updateXmlCfgs:
        print('>> Updating XML Cfgs')
        for vol in VOLUMES:
            print('>>>>>>>>Creating/Updating Volume: {}'.format(vol))
            updateVolumes.updateCfgs('{}/{}'.format(PATH_VOLUMES, vol))
    if ARGS.dockerCompose:
        print('>> Generating YML File')
        generateDockerComposeFile(DOCKER_COMPOSE_FILENAME, False)
    if ARGS.dockerComposeAuth:
        print('>> Generating YML File')
        generateDockerComposeFile(DOCKER_COMPOSE_FILENAME, True)
    if ARGS.updateVolumes:
        print('>> Create/Update Volumes')
        extractJenkinsHome()
        for vol in VOLUMES:
            print('>>>>>>>> Updating Cfgs Volume: {}'.format(vol))
            updateVolumes.updateVolume(BASE_VOLUME + DEFAULT_JENKINS_HOME, PATH_VOLUMES, vol, oldVolumePath=None, deleteDir=False)
    print('DONE')
