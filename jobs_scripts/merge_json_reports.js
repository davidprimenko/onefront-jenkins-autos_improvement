const fs = require('fs');

const filename = process.argv[2] || '';
const filepaths = process.argv[3] || '';
const browser = process.argv[4] || '';

let final_report = {'tests': []};
const features = {};

filepaths.split(',').forEach((path) => {
    let data;
    try {
        data = JSON.parse(fs.readFileSync(path, 'utf-8'));
    } catch (e) {
        throw e;
    }

    data['tests'].forEach((obj) => {
        if (features[obj['feature']]) {
            features[obj['feature']] = features[obj['feature']].concat(obj['scenarios']);
        } else {
            features[obj['feature']] = obj['scenarios'];
        }
    });
});

Object.keys(features).forEach((featureName) => {
    final_report.tests.push({
        'feature': featureName,
        'scenarios': features[featureName]
    });
});

fs.writeFileSync(filename, JSON.stringify(final_report));

console.info('>>', browser.toUpperCase(),'FINAL REPORT');
console.info('On Jenkins: --jsonTest='.concat(JSON.stringify(final_report)),'\n');
console.info('On Linux: --jsonTest=\''.concat(JSON.stringify(final_report),'\'\n'));
console.info('Or, download the file '.concat(filename, ' and execute --jsonFile=', filename));
