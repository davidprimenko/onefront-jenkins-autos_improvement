#!/usr/bin/env python3

import json
import xml.etree.ElementTree
from xml.dom import minidom

from pprint import pprint


LOG_DEBUG = -1
LOG_INFO = 0
LOG_WARN = 1
LOG_ERR = 2
LOG_ALL = 3

CLOUD_VERSION = 'ec2@1.40-SNAPSHOT'
CLOUD_CFG_FILE = 'config.xml'
JENKINS_LOCAL_CFG = 'jenkins.model.JenkinsLocationConfiguration.xml'
JSON_VOLUMES_CFG_FILE = 'default_config.json'
JSON_VOLUMES_KEY = 'Volumes'
VERBOSE = LOG_ALL

# XML KEYS
XML_EC2_CLOUD = 'hudson.plugins.ec2.EC2Cloud'
XML_EC2_NAME = 'name'
XML_EC2_TAGS = 'tags'
XML_EC2_TAGS_NAME = 'name' 
XML_EC2_TAGS_VALUE = 'value' 
XML_EC2_AMI_ID = 'ami'
XML_EC2_INST_CAP = 'instanceCap'
XML_EC2_INST_TYPE = 'type'
XML_EC2_SUBNETID = 'subnetId'
XML_EC2_DESCRIPTION = 'description'
XML_LOCAL = 'jenkins.model.JenkinsLocationConfiguration'
XML_LOCAL_MAIL = 'adminAddress'
XML_LOCAL_URL = 'jenkinsUrl'

# JSON KEYS
JSON_EC2_CLOUD = 'ec2_cloud'
JSON_EC2_NAME = 'name'
JSON_EC2_TAGS = 'tags' 
JSON_EC2_TAGS_NAME = 'name' 
JSON_EC2_TAGS_VALUE = 'value' 
JSON_EC2_AMI_ID = 'ami_id'
JSON_EC2_INST_CAP = 'instance_cap'
JSON_EC2_INST_TYPE = 'instance_type'
JSON_EC2_SUBNETID = 'subnetId'
JSON_EC2_DESCRIPTION = 'description'
JSON_LOCAL_MAIL = 'admin_mail'
JSON_LOCAL_PORT = 'port'
JSON_LOCAL_NAME = 'name'
JSON_LOCAL_SERVER = 'server'


def log(message, lvl):
    if (VERBOSE == lvl | VERBOSE == LOG_ALL):
        print(message) 

def getDefaultCfgFile(filename):
    return json.load(open(filename, 'r+'))

def getDefaultXmlJenkinsFile(filename):
   return minidom.parse(filename)

def setNewCfgFile(minidom_data, filename):
    minidom_data.writexml(open(filename, 'w+'))

def updateNodeText(node, xmlKey, jsonValue, pos=0):
    try:
        if (node.getElementsByTagName(xmlKey)[pos].firstChild.data != jsonValue):
                (node.getElementsByTagName(xmlKey)[pos].firstChild.replaceWholeText(jsonValue))
    except:
        log('Failed to find node {}'.format(xmlKey), LOG_ERR)


def editCloudSettings(jsonCfg, xmlCfg):
    cloudSettings = xmlCfg.getElementsByTagName(XML_EC2_CLOUD)
    for cloud in cloudSettings:
        cloudVersion = cloud.getAttribute('plugin')
        if (cloudVersion != CLOUD_VERSION):
            log("WARN: You are using {} and the default is {}".format(cloudVersion,CLOUD_VERSION), LOG_WARN)

        log('########################## CLOUD SETTINGS ##########################', LOG_INFO)

        log('### EC2 AMAZON NAME: {}'.format(jsonCfg[JSON_EC2_CLOUD][JSON_EC2_NAME]), LOG_INFO)
        updateNodeText(cloud, XML_EC2_NAME, jsonCfg[JSON_EC2_CLOUD][JSON_EC2_NAME])

        log('### DESCRIPTION: {}'.format(jsonCfg[JSON_EC2_CLOUD][JSON_EC2_DESCRIPTION]), LOG_INFO)
        updateNodeText(cloud, XML_EC2_DESCRIPTION, jsonCfg[JSON_EC2_CLOUD][JSON_EC2_DESCRIPTION])

        log('### AMI ID: {}'.format(jsonCfg[JSON_EC2_CLOUD][JSON_EC2_AMI_ID]), LOG_INFO)
        updateNodeText(cloud, XML_EC2_AMI_ID, jsonCfg[JSON_EC2_CLOUD][JSON_EC2_AMI_ID])
        
        log('### INSTANCES CAP: {}'.format(jsonCfg[JSON_EC2_CLOUD][JSON_EC2_INST_CAP]), LOG_INFO)
        updateNodeText(cloud, XML_EC2_INST_CAP, jsonCfg[JSON_EC2_CLOUD][JSON_EC2_INST_CAP], 1)

        log('### INSTANCES TYPE: {}'.format(jsonCfg[JSON_EC2_CLOUD][JSON_EC2_INST_TYPE]), LOG_INFO)
        updateNodeText(cloud, XML_EC2_INST_TYPE, jsonCfg[JSON_EC2_CLOUD][JSON_EC2_INST_TYPE])

        log('### SUBNET_ID: {}'.format(jsonCfg[JSON_EC2_CLOUD][JSON_EC2_SUBNETID]), LOG_INFO)
        updateNodeText(cloud, XML_EC2_SUBNETID, jsonCfg[JSON_EC2_CLOUD][JSON_EC2_SUBNETID])


        cloudTags = xmlCfg.getElementsByTagName(XML_EC2_TAGS)
        for tag in cloudTags:
            for jsonTag in jsonCfg[JSON_EC2_CLOUD][JSON_EC2_TAGS]:
                if (tag.getElementsByTagName(XML_EC2_TAGS_NAME)[0].firstChild.data == jsonTag[JSON_EC2_TAGS_NAME]):
                    log('### TAGs : {} -- {}'.format(jsonTag[JSON_EC2_TAGS_NAME], jsonTag[JSON_EC2_TAGS_VALUE]), LOG_INFO)
                    updateNodeText(tag, XML_EC2_TAGS_VALUE, jsonTag[JSON_EC2_TAGS_VALUE])



def editLocalSettings(jsonCfg, xmlCfg):
    localSettings = xmlCfg.getElementsByTagName(XML_LOCAL)
    log('########################## LOCAL SETTINGS ##########################', LOG_INFO)
    for lc in localSettings:
        log('### ADMIN MAIL: {}'.format(jsonCfg[JSON_LOCAL_MAIL]), LOG_INFO)
        updateNodeText(lc, XML_LOCAL_MAIL, jsonCfg[JSON_LOCAL_MAIL])

        localUrl = 'http://{}:{}/'.format(jsonCfg[JSON_LOCAL_SERVER],jsonCfg[JSON_LOCAL_PORT])
        log('### JENKINS URL: {}'.format(localUrl), LOG_INFO)
        updateNodeText(lc, XML_LOCAL_URL, localUrl)

def applyCfgs(jsonVolumeCfg, volumePath):   
    # General Config
    genFilename = '{}/{}'.format(volumePath, CLOUD_CFG_FILE)
    generalXmlCfg = getDefaultXmlJenkinsFile(genFilename)
    editCloudSettings(jsonVolumeCfg, generalXmlCfg)
    setNewCfgFile(generalXmlCfg, genFilename)

    # Local Config
    localFilename = '{}/{}'.format(volumePath, JENKINS_LOCAL_CFG)
    localXmlCfg = getDefaultXmlJenkinsFile(localFilename)
    editLocalSettings(jsonVolumeCfg, localXmlCfg)
    setNewCfgFile(localXmlCfg, localFilename)

if __name__ == '__main__':
    jsonCfg = getDefaultCfgFile(JSON_VOLUMES_CFG_FILE)
    for jsonVolumeCfg in jsonCfg[JSON_VOLUMES_KEY]:
        applyCfgs(jsonVolumeCfg, jsonVolumeCfg['name'])



