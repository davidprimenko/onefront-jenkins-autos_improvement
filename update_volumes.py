#!/usr/bin/env python3
import os
from shutil import copy2, copytree, rmtree, copy, move
import update_volume_configs as applyVolCfg
from argparse import ArgumentParser

# Default Cfg file
JSON_VOLUMES_CFG_FILE = applyVolCfg.JSON_VOLUMES_CFG_FILE

def parseArgs():
    parser = ArgumentParser()
    parser.add_argument('--deleteDir', dest='deleteDir', type=bool, 
                        help='Delete old directory after copy')
    parser.add_argument('--baseVolume', dest='baseVol', type=str, 
                        help='Path+Name of the default volume')
    parser.add_argument('--updateVolumeName', dest='updateVolName', type=str, 
                        help='Name of the new or volume to be updated')
    parser.add_argument('--updateVolumePath', dest='updateVolPath', type=str, 
                        help='Path of the new or volume to be updated')

    return parser.parse_args()

def updateVolume(volumeToCopyPath, volumeToUpdatePath, volumeToUpdateName, oldVolumePath=None, deleteDir=False):
    newVolumeName = '{}/{}'.format(volumeToUpdatePath,volumeToUpdateName)
    if not os.path.exists(newVolumeName):
        newVolume = True    
        print('#### Creating New Volume')
        newVolumeName = createNewVolume(volumeToUpdatePath,volumeToUpdateName, prefix='new_')
    print('#### Copying Default Files')
    copyDefaultFiles(volumeToCopyPath,newVolumeName)
    print('#### Copying Job Config Files')
    copyJobCfgFiles(volumeToCopyPath,newVolumeName)
    print('#### Updating Configs')
    updateCfgs(newVolumeName, volumeCfgFile=JSON_VOLUMES_CFG_FILE)

    if oldVolumePath:
        print('#### Copying History')
        moveJobsHistory(oldVolumePath, newVolumeName)
        if deleteDir:
            print('#### Removing Old Volume')
            removeOldVolume(volumeToCopyPath)
    if newVolume:
        print('#### Renaming New Volume')
        renameNewVolume(newVolumeName, prefix='new_', setFilePerms=True)
    print('#### DONE ####')

def setFilePermission(path):
    os.chown(path, 1000, 1000)
    for root, dirs, files in os.walk(path):
        try: 
            for momo in dirs:  
                os.chown(os.path.join(root, momo), 1000, 1000)
            for momo in files:
                os.chown(os.path.join(root, momo), 1000, 1000)
        except:
            print('Failed to change permission')

def createNewVolume(volumePath, volumeName, prefix='new_'):
    dirPathName = '{}/{}{}/'.format(volumePath,prefix,volumeName)
    if not os.path.exists(dirPathName):
        os.makedirs(dirPathName)
    return dirPathName

def removeOldVolume(volumePath):
    rmtree(volumePath, ignore_errors=False, onerror=None)

def renameNewVolume(newVolumeName, prefix='new_', setFilePerms=False):
    volumeName = newVolumeName.replace(prefix, '')
    os.rename(newVolumeName, volumeName)
    if setFilePerms:
        print('#### Setting File Permissions')
        setFilePermission(volumeName)

def checkExcludedDirs(filename):
    if filename == 'nodes':
        return False
    elif filename == 'jobs':
        return False
    elif filename == 'logs':
        return False
    elif filename == 'monitoring':
        return False
    return True

def recursiveCopy(src,dest):
    for filename in os.listdir(src):
        if checkExcludedDirs(filename):
            newSrc = os.path.join(src,filename)
            if os.path.isdir(newSrc):
                newDest = '{}/{}'.format(dest, filename)
                os.makedirs(newDest)
                recursiveCopy(newSrc, newDest)
            else:
                copy2(newSrc, dest)
            

def copyDefaultFiles(src, dest, symlinks=False, ignore=None):
    recursiveCopy(src, dest)

##Copy cfg.xml
def copyJobCfgFiles(src, dest):
    jobsPath = '{}/jobs'.format(src)
    os.makedirs('{}/jobs'.format(dest))
    for job in os.listdir(jobsPath):
        os.makedirs('{}/jobs/{}'.format(dest,job))
        for filename in os.listdir('{}/{}'.format(jobsPath, job)):
            if not os.path.isdir('{}/jobs/{}/{}'.format(jobsPath, job, filename)):
                try:
                    copy('{}/{}/{}'.format(jobsPath, job, filename),('{}/jobs/{}/'.format(dest, job)))
                except:
                    print('Sometimes something cannot be copied')

def updateCfgs(volumeName, volumeCfgFile=JSON_VOLUMES_CFG_FILE):
    jsonCfg = applyVolCfg.getDefaultCfgFile(volumeCfgFile)
    for jsonVolumeCfg in jsonCfg[applyVolCfg.JSON_VOLUMES_KEY]:
        if jsonVolumeCfg['name'] in volumeName:
            applyVolCfg.applyCfgs(jsonVolumeCfg, volumeName)


def moveJobsHistory(volumeLegacy, relativeDest, symlinks=False, ignore=None):
    for jobName in os.listdir('{}/jobs'.format(volumeLegacy)):
        move(os.path.join('{}/jobs/{}/builds'.format(volumeLegacy, jobName)), os.path.join('{}/jobs/{}'.format(relativeDest, jobName)))

if __name__ == '__main__':
    ARGS = parseArgs()
    baseVol = os.path.join(ARGS.baseVol)
    updateVolName = os.path.join(ARGS.updateVolName)
    updateVolPath = os.path.join(ARGS.updateVolPath)
    deleteDir = ARGS.deleteDir    
    updateVolume(baseVol, updateVolPath, updateVolName, deleteDir=deleteDir) 
